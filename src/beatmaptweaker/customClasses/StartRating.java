package beatmaptweaker.customClasses;

import beatmaptweaker.menus.MultipleRate;

public class StartRating implements Runnable {

    private final MultipleRate sourceFrame;
    
    public StartRating(MultipleRate sourceFrame) {
        this.sourceFrame = sourceFrame;
    }
    
    @Override
    public void run() {
        this.sourceFrame.startRating();
    }

}