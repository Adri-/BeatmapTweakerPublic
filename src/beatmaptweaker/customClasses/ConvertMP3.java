package beatmaptweaker.customClasses;

import java.io.IOException;
import java.util.ArrayList;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author ADRIEN
 */
abstract public class ConvertMP3 {

    public static void createMp3(String input, String output, float rate, boolean nightcore) throws IOException {
        String command = "";
        if (!nightcore) {

            String atempo = "";
            ArrayList<Float> mults = new ArrayList<>();

            if (rate > 2) {
                float lastmult = rate;

                do {
                    lastmult /= 2;
                    mults.add((float) 2.0);
                } while (lastmult > 2);

                mults.add(lastmult);
            } else if (rate < 0.5) {
                float lastmult = rate;

                do {
                    lastmult *= 2;
                    mults.add((float) 0.5);
                } while (lastmult < 0.5);

                mults.add(lastmult);
            } else {
                mults.add(rate);
            }

            int count = 0;
            for (float tempo : mults) {
                if (count > 0) {
                    atempo += ",atempo=" + Float.toString(tempo);
                } else {
                    atempo += "atempo=" + Float.toString(tempo);
                }
                count++;
            }

            command = "\"./ffmpeg/bin/ffmpeg.exe\" -y -i \"" + input + "\" -filter:a \"" + atempo + "\" -vn \"" + output + "\"";
        } else {
            command = "\"./ffmpeg/bin/ffmpeg.exe\" -y -i \"" + input + "\" -filter:a \"asetrate=" + Float.toString(44100 * rate) + "\" -vn \"" + output + "\"";
        }

        Runtime rt = Runtime.getRuntime();

        System.out.println(command);
        rt.exec(command);
    }
}
