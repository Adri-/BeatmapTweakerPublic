package beatmaptweaker.customClasses;

/**
 *
 * @author adrien
 */
public class HitObject {

    private String generalType;

    // General
    private int x;
    private int y;
    private int time;
    private int type;
    private int hitsound;
    private String addition;

    // Spinner
    private int endTime;

    // Slider    
    private int sliderType;
    private String sliderMisc;

    public HitObject(String line) {
        String[] data = line.split(",");

        this.x = Integer.valueOf(data[0]);
        this.y = Integer.valueOf(data[1]);
        this.time = Integer.valueOf(data[2]);
        this.type = Integer.valueOf(data[3]);
        this.hitsound = Integer.valueOf(data[4]);

        if (data.length <= 6) {
            // HitObject
            this.generalType = "HitObject";

            if (data.length == 6) {
                String last = data[5];

                String[] dataD = last.split(":");

                if (dataD.length == 1) {
                    // Spinner
                    this.generalType = "Spinner";

                    this.endTime = Integer.valueOf(data[5]);
                } else if (!"0".equals(dataD[0])) {
                    this.generalType = "HitObjectManiaSlider";
                    this.endTime = Integer.valueOf(dataD[0]);

                    int count = 0;
                    this.addition = ":";
                    for (String word : dataD) {
                        if (count > 0) {
                            this.addition += word + ":";
                        }
                        count++;
                    }
                } else if ("0".equals(dataD[0])) {
                    this.addition = "0:";
                    for (String word : dataD) {
                        this.addition += word + ":";
                    }
                }
            }
        } else if (data.length == 7) {
            // Spinner
            this.generalType = "Spinner";

            this.endTime = Integer.valueOf(data[5]);
            this.addition = data[6];
        } else {
            // Slider
            this.generalType = "Slider";

            int count = 0;
            this.sliderMisc = "";
            for (String word : data) {
                if (count > 4) {
                    this.sliderMisc += "," + word;
                }
                count++;
            }
        }
    }

    public void rateOffset(float rate, int decal) {
        this.setTime((int) ((this.getTime() + decal) / rate));

        if ("Spinner".equals(this.generalType) || "HitObjectManiaSlider".equals(this.generalType)) {
            this.setEndTime((int) ((this.getEndTime() + decal) / rate));
        }
    }

    @Override
    public String toString() {
        String returnString = "";

        if (null != this.generalType) {
            switch (this.generalType) {
                case "HitObject":
                    // x,y,time,type,hitsound,ad:di:ti:on:
                    returnString = Integer.toString(this.x) + ","
                            + Integer.toString(this.y) + ","
                            + Integer.toString(this.time) + ","
                            + Integer.toString(this.type) + ","
                            + Integer.toString(this.hitsound);

                    if (null != this.addition) {
                        returnString += "," + this.addition;
                    }

                    break;
                case "HitObjectManiaSlider":
                    // x,y,time,type,hitsound,endTime:ad:di:ti:on:
                    returnString = Integer.toString(this.x) + ","
                            + Integer.toString(this.y) + ","
                            + Integer.toString(this.time) + ","
                            + Integer.toString(this.type) + ","
                            + Integer.toString(this.hitsound) + ","
                            + Integer.toString(this.endTime);

                    if (null != this.addition) {
                        returnString += this.addition;
                    }
                    break;
                case "Slider":
                    // x,y,time,type,hitsound,sp|in:er,m,i|s:sc:
                    returnString = Integer.toString(this.x) + ","
                            + Integer.toString(this.y) + ","
                            + Integer.toString(this.time) + ","
                            + Integer.toString(this.type) + ","
                            + Integer.toString(this.hitsound)
                            + this.sliderMisc;
                    break;
                case "Spinner":
                    // x,y,time,type,hitsount,endTime,ad:di:ti:on:
                    returnString = Integer.toString(this.x) + ","
                            + Integer.toString(this.y) + ","
                            + Integer.toString(this.time) + ","
                            + Integer.toString(this.type) + ","
                            + Integer.toString(this.hitsound) + ","
                            + Integer.toString(this.endTime);

                    if (null != this.addition) {
                        returnString += "," + this.addition;
                    }

                    break;
            }
        }

        return returnString;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public int getTime() {
        return time;
    }

    public void setTime(int time) {
        this.time = time;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public int getHitsound() {
        return hitsound;
    }

    public void setHitsound(int hitsound) {
        this.hitsound = hitsound;
    }

    public String getAddition() {
        return addition;
    }

    public void setAddition(String addition) {
        this.addition = addition;
    }

    public int getEndTime() {
        return endTime;
    }

    public void setEndTime(int endTime) {
        this.endTime = endTime;
    }

    public int getSliderType() {
        return sliderType;
    }

    public void setSliderType(int sliderType) {
        this.sliderType = sliderType;
    }

    public String getSpinnerMisc() {
        return sliderMisc;
    }

    public void setSpinnerMisc(String sliderMisc) {
        this.sliderMisc = sliderMisc;
    }

}
