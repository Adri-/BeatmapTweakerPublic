package beatmaptweaker.customClasses;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Arrays;

/**
 *
 * @author adrien
 */
public final class Beatmap {

    private String filename = "";
    private ArrayList<String> originalLines = new ArrayList();
    private ArrayList<String> currentLines = new ArrayList();

    private int id;

    // General
    private String AudioFileName = "";
    private int AudioLeadIn = 0;
    private int PreviewTime = 0;

    private boolean Countdown = false;
    private String SampleSet = "";
    private float StackLeniency = 0;
    private int Mode = 0;
    private boolean LetterboxInBreaks = false;
    private boolean EpilepsyWarning = false;
    private boolean SpecialStyle = false;
    private boolean WidescreenStoryboard = false;

    // Editor
    private ArrayList Bookmarks = new ArrayList<>();
    private float DistanceSpacing = 0;
    private int BeatDivisor = 0;
    private int GridSize = 0;
    private float TimelineZoom = 0;

    // MetaData
    private String Title = "";
    private String TitleUnicode = "";
    private String Artist = "";
    private String ArtistUnicode = "";
    private String Creator = "";
    private String Version = "";
    private String Source = "";
    private ArrayList<String> Tags = new ArrayList<>();
    private int BeatmapID = 0;
    private int BeatmapSetID = 0;

    // Difficulty
    private float HPDrainRate = 0;
    private float CircleSize = 0;
    private float OverallDifficulty = 0;
    private float ApproachRate = 0;
    private float SliderMultiplier = 0;
    private float SliderTickRate = 0;

    // Events (basic handling)
    private ArrayList<String> EventLines = new ArrayList<>();

    // TimingPoints (basic handling)
    private ArrayList<TimingPoint> TimingPoints = new ArrayList<>();

    // Colours
    private ArrayList<String> Colors = new ArrayList<>();

    // HitObjects
    private ArrayList<HitObject> HitObjects = new ArrayList<>();

    // Convert phases
    private int phase;

    public Beatmap() {

    }

    public Beatmap(File file) throws IOException {
        this(file.getName(),(ArrayList) Files.readAllLines(file.toPath(), StandardCharsets.UTF_8));
    }

    public Beatmap(String filename, ArrayList<String> lines) {
        this.originalLines = lines;
        this.currentLines = this.originalLines;
        this.filename = filename;

        ////// PHASES ////
        // 0 data
        // 1 timing
        // 2 color data
        // 3 hitobjects
        // 4 events
        this.phase = 0;

        for (String line : lines) {

            // Check Phase
            if (line.contains("[TimingPoints]")) {
                this.phase = 1;
            } else if (line.contains("[Colours]")) {
                this.phase = 2;
            } else if (line.contains("[HitObjects]")) {
                this.phase = 3;
            } else if (line.contains("[Events]")) {
                this.phase = 4;
            }

            String[] elements = line.split(":");
            // General
            if (this.phase == 0 && elements.length > 1) {
                if (line.contains("AudioFilename:")) {
                    this.setAudioFileName(elements[1].trim());
                } else if (line.contains("AudioLeadIn:")) {
                    this.setAudioLeadIn(Integer.valueOf(elements[1].trim()));
                } else if (line.contains("PreviewTime:")) {
                    this.setPreviewTime(Integer.valueOf(elements[1].trim()));
                } else if (line.contains("Countdown:")) {
                    this.setCountdown(!"0".equals(elements[1].trim()));
                } else if (line.contains("SampleSet:")) {
                    this.setSampleSet(elements[1].trim());
                } else if (line.contains("StackLeniency:")) {
                    this.setStackLeniency(Float.valueOf(elements[1].trim()));
                } else if (line.contains("Mode:")) {
                    this.setMode(Integer.valueOf(elements[1].trim()));
                } else if (line.contains("LetterboxInBreaks:")) {
                    this.setLetterboxInBreaks(!"0".equals(elements[1].trim()));
                } else if (line.contains("EpilepsyWarning:")) {
                    this.setLetterboxInBreaks(!"0".equals(elements[1].trim()));
                } else if (line.contains("SpecialStyle:")) {
                    this.setSpecialStyle(!"0".equals(elements[1].trim()));
                } else if (line.contains("WidescreenStoryboard:")) {
                    this.setWidescreenStoryboard(!"0".equals(elements[1].trim()));
                } // Editor
                else if (line.contains("DistanceSpacing:")) {
                    this.setDistanceSpacing(Float.valueOf(elements[1].trim()));
                } else if (line.contains("BeatDivisor:")) {
                    this.setBeatDivisor(Integer.valueOf(elements[1].trim()));
                } else if (line.contains("GridSize:")) {
                    this.setGridSize(Integer.valueOf(elements[1].trim()));
                } else if (line.contains("TimelineZoom:")) {
                    this.setTimelineZoom(Float.valueOf(elements[1].trim()));
                } // Meta
                else if (line.contains("Title:")) {
                    this.setTitle(elements[1].trim());
                } else if (line.contains("TitleUnicode:")) {
                    this.setTitleUnicode(elements[1].trim());
                } else if (line.contains("Artist:")) {
                    this.setArtist(elements[1].trim());
                } else if (line.contains("ArtistUnicode:")) {
                    this.setArtistUnicode(elements[1].trim());
                } else if (line.contains("Creator:")) {
                    this.setCreator(elements[1].trim());
                } else if (line.contains("Version:")) {
                    this.setVersion(elements[1].trim());
                } else if (line.contains("Source:")) {
                    this.setSource(elements[1].trim());
                } else if (line.contains("Tags:")) {
                    this.setTags(new ArrayList(Arrays.asList(elements[1].split(" "))));
                } else if (line.contains("BeatmapID:")) {
                    this.setBeatmapID(Integer.valueOf(elements[1].trim()));
                } else if (line.contains("BeatmapSetID:")) {
                    this.setBeatmapSetID(Integer.valueOf(elements[1].trim()));
                } else if (line.contains("HPDrainRate:")) {
                    this.setHPDrainRate(Float.valueOf(elements[1].trim()));
                } else if (line.contains("CircleSize:")) {
                    this.setCircleSize(Float.valueOf(elements[1].trim()));
                } else if (line.contains("OverallDifficulty:")) {
                    this.setOverallDifficulty(Float.valueOf(elements[1].trim()));
                } else if (line.contains("ApproachRate:")) {
                    this.setApproachRate(Float.valueOf(elements[1].trim()));
                } else if (line.contains("SliderMultiplier:")) {
                    this.setSliderMultiplier(Float.valueOf(elements[1].trim()));
                } else if (line.contains("SliderTickRate:")) {
                    this.setSliderTickRate(Float.valueOf(elements[1].trim()));
                }
            }
            if (this.phase == 1 && !"[TimingPoints]".equals(line) && !"".equals(line)) {
                TimingPoint timingpoint = new TimingPoint(line);
                this.TimingPoints.add(timingpoint);
            }
            if (this.phase == 2 && !"[Colours]".equals(line) && !"".equals(line)) {
                this.Colors.add(line);
            }
            if (this.phase == 3 && !"[HitObjects]".equals(line) && !"".equals(line)) {
                HitObject hitobject = new HitObject(line);
                this.HitObjects.add(hitobject);
            }
            if (this.phase == 4 && !"[Events]".equals(line) && !"".equals(line)) {
                this.EventLines.add(line);
            }
        }
    }

    public Beatmap getRatedBeatmap(float rate , int decal) {
        String rateLabel = " x" + Float.toString(rate);
        String fileName = "Extension error";
        
        int pos = this.filename.lastIndexOf(".");
        if (pos > 0) {
            fileName = this.filename.substring(0, pos);
        }
        
        ArrayList<String> newLines = (ArrayList<String>) this.originalLines.clone();
        
        Beatmap returnedBeatmap = new Beatmap(fileName, newLines);
        
        returnedBeatmap.setVersion(this.getVersion()+rateLabel);
        returnedBeatmap.setFilename(fileName + rateLabel + ".osu");
        
        if(returnedBeatmap.getMode()==0) {
            returnedBeatmap.setApproachRate(returnedBeatmap.getApproachRate()*rate);
        }

        returnedBeatmap.rateHitObjects(rate, decal);
        returnedBeatmap.rateTimingPoints(rate, decal);
        returnedBeatmap.setPreviewTime((int) (returnedBeatmap.getPreviewTime()/rate));
        
        return returnedBeatmap;
    }

    public void writeToFile(File target) throws IOException {
        this.writeItself();
        BufferedWriter outputWriter = null;
        outputWriter = new BufferedWriter(new FileWriter(target));

        for (String line : this.currentLines) {
            outputWriter.write(line);
            outputWriter.newLine();
        }
        outputWriter.flush();
        outputWriter.close();
    }

    public void writeItself() {
        this.currentLines.clear();

        this.currentLines.add("osu file format v14");
        this.currentLines.add("");

        this.currentLines.add("[General]");
        this.currentLines.add("AudioFilename: " + this.getAudioFileName());
        this.currentLines.add("AudioLeadIn: " + Integer.toString(this.getAudioLeadIn()));
        this.currentLines.add("PreviewTime: " + Integer.toString(this.getPreviewTime()));
        this.currentLines.add("Countdown: " + (this.isCountdown() ? "1" : "0"));
        this.currentLines.add("SampleSet: " + this.getSampleSet());
        this.currentLines.add("StackLeniency: " + Float.toString(this.getStackLeniency()));
        this.currentLines.add("Mode: " + Integer.toString(this.getMode()));
        this.currentLines.add("LetterboxInBreaks: " + (this.isLetterboxInBreaks() ? "1" : "0"));
        this.currentLines.add("EpilepsyWarning: " + (this.isLetterboxInBreaks() ? "1" : "0"));
        this.currentLines.add("SpecialStyle: " + (this.isSpecialStyle() ? "1" : "0"));
        this.currentLines.add("WidescreenStoryboard: " + (this.isWidescreenStoryboard() ? "1" : "0"));
        this.currentLines.add("");
        this.currentLines.add("[Editor]");
        this.currentLines.add("DistanceSpacing: " + Float.toString(this.getDistanceSpacing()));
        this.currentLines.add("BeatDivisor: " + Integer.toString(this.getBeatDivisor()));
        this.currentLines.add("GridSize: " + Integer.toString(this.getGridSize()));
        this.currentLines.add("TimelineZoom: " + Float.toString(this.getTimelineZoom()));
        this.currentLines.add("");
        this.currentLines.add("[Metadata]");
        this.currentLines.add("Title: " + this.getTitle());
        this.currentLines.add("TitleUnicode: " + this.getTitleUnicode());
        this.currentLines.add("Artist: " + this.getArtist());
        this.currentLines.add("ArtistUnicode: " + this.getArtistUnicode());
        this.currentLines.add("Creator: " + this.getCreator());
        this.currentLines.add("Version: " + this.getVersion());
        this.currentLines.add("Source: " + this.getSource());

        String tags = "";
        tags = this.getTags().stream().map((tag) -> tag + " ").reduce(tags, String::concat);

        this.currentLines.add("Tags: " + tags);
        this.currentLines.add("BeatmapID: " + Integer.toString(this.getBeatmapID()));
        this.currentLines.add("BeatmapSetID: " + Integer.toString(this.getBeatmapSetID()));
        this.currentLines.add("");
        this.currentLines.add("[Difficulty]");
        this.currentLines.add("HPDrainRate: " + this.getHPDrainRate());
        this.currentLines.add("CircleSize: " + this.getCircleSize());
        this.currentLines.add("OverallDifficulty: " + this.getOverallDifficulty());
        this.currentLines.add("ApproachRate: " + this.getApproachRate());
        this.currentLines.add("SliderMultiplier: " + this.getSliderMultiplier());
        this.currentLines.add("SliderTickRate: " + this.getSliderTickRate());
        this.currentLines.add("");
        this.currentLines.add("[Events]");
        for (String line : EventLines) {
            this.currentLines.add(line);
        }
        this.currentLines.add("");
        this.currentLines.add("[Colours]");
        for (String line : Colors) {
            this.currentLines.add(line);
        }
        this.currentLines.add("");
        this.currentLines.add("[TimingPoints]");
        for (TimingPoint timingPoint : TimingPoints) {
            this.currentLines.add(timingPoint.toString());
        }
        this.currentLines.add("");
        this.currentLines.add("[HitObjects]");
        for (HitObject hitObject : HitObjects) {
            this.currentLines.add(hitObject.toString());
        }
    }

    public void takeModel(Beatmap model) {
        this.setVersion(this.Artist + " - " + this.Title + " ("+ this.Creator +") " + "["+this.Version+"]");
        this.setTitle(model.getTitle());
        this.setTitleUnicode(model.getTitleUnicode());
        this.setArtist(model.getArtist());
        this.setArtistUnicode(model.getArtistUnicode());
        this.setBeatmapID(0);
        this.setBeatmapSetID(0);
        this.setFilename(this.Artist + " - " + this.Title + " ("+ this.Creator +") " + "["+this.Version+"].osu");
        this.setCreator(model.getCreator());
        this.writeItself();
    }
    
    public String getBackgroundName() {
        String r = null;
        for(String line: this.EventLines) {
            if(line.contains(".png") || line.contains(".jpeg") || line.contains(".jpg")) {
                r = line.split(",")[2];
                return r.substring(1,r.length()-1);
            }
        }
        return r;
    }
    
    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return this.id;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public String getFilename() {
        return this.filename;
    }

    public void rateHitObjects(float rate, int decal) {
        this.HitObjects.stream().forEach((hit) -> {
            hit.rateOffset(rate, decal);
        });
    }

    public void rateTimingPoints(float rate, int decal) {
        this.TimingPoints.stream().forEach((tp) -> {
            tp.rateOffset(rate, decal);
        });
    }

    public void rateTimingPoints() {

    }

    public String getAudioFileName() {
        return AudioFileName;
    }

    public void setAudioFileName(String AudioFileName) {
        this.AudioFileName = AudioFileName;
    }

    public int getAudioLeadIn() {
        return AudioLeadIn;
    }

    public void setAudioLeadIn(int AudioLeadIn) {
        this.AudioLeadIn = AudioLeadIn;
    }

    public int getPreviewTime() {
        return PreviewTime;
    }

    public void setPreviewTime(int PreviewTime) {
        this.PreviewTime = PreviewTime;
    }

    public boolean isCountdown() {
        return Countdown;
    }

    public void setCountdown(boolean Countdown) {
        this.Countdown = Countdown;
    }

    public String getSampleSet() {
        return SampleSet;
    }

    public void setSampleSet(String SampleSet) {
        this.SampleSet = SampleSet;
    }

    public float getStackLeniency() {
        return StackLeniency;
    }

    public void setStackLeniency(float StackLeniency) {
        this.StackLeniency = StackLeniency;
    }

    public int getMode() {
        return Mode;
    }

    public void setMode(int Mode) {
        this.Mode = Mode;
    }

    public boolean isLetterboxInBreaks() {
        return LetterboxInBreaks;
    }

    public void setLetterboxInBreaks(boolean LetterboxInBreaks) {
        this.LetterboxInBreaks = LetterboxInBreaks;
    }

    public boolean isSpecialStyle() {
        return SpecialStyle;
    }

    public void setSpecialStyle(boolean SpecialStyle) {
        this.SpecialStyle = SpecialStyle;
    }

    public boolean isWidescreenStoryboard() {
        return WidescreenStoryboard;
    }

    public void setWidescreenStoryboard(boolean WidescreenStoryboard) {
        this.WidescreenStoryboard = WidescreenStoryboard;
    }

    public ArrayList getBookmarks() {
        return Bookmarks;
    }

    public void setBookmarks(ArrayList Bookmarks) {
        this.Bookmarks = Bookmarks;
    }

    public float getDistanceSpacing() {
        return DistanceSpacing;
    }

    public void setDistanceSpacing(float DistanceSpacing) {
        this.DistanceSpacing = DistanceSpacing;
    }

    public float getTimelineZoom() {
        return TimelineZoom;
    }

    public void setTimelineZoom(float TimelineZoom) {
        this.TimelineZoom = TimelineZoom;
    }

    public int getBeatDivisor() {
        return BeatDivisor;
    }

    public void setBeatDivisor(int BeatDivisor) {
        this.BeatDivisor = BeatDivisor;
    }

    public int getGridSize() {
        return GridSize;
    }

    public void setGridSize(int GridSize) {
        this.GridSize = GridSize;
    }

    public String getTitle() {
        return Title;
    }

    public void setTitle(String Title) {
        this.Title = Title;
    }

    public String getTitleUnicode() {
        return TitleUnicode;
    }

    public void setTitleUnicode(String TitleUnicode) {
        this.TitleUnicode = TitleUnicode;
    }

    public String getArtist() {
        return Artist;
    }

    public void setArtist(String Artist) {
        this.Artist = Artist;
    }

    public String getArtistUnicode() {
        return ArtistUnicode;
    }

    public void setArtistUnicode(String ArtistUnicode) {
        this.ArtistUnicode = ArtistUnicode;
    }

    public String getCreator() {
        return Creator;
    }

    public void setCreator(String Creator) {
        this.Creator = Creator;
    }

    public String getVersion() {
        return Version;
    }

    public void setVersion(String Version) {
        this.Version = Version;
    }

    public String getSource() {
        return Source;
    }

    public void setSource(String Source) {
        this.Source = Source;
    }

    public ArrayList<String> getTags() {
        return Tags;
    }

    public void setTags(ArrayList Tags) {
        this.Tags = Tags;
    }

    public int getBeatmapID() {
        return BeatmapID;
    }

    public void setBeatmapID(int BeatmapID) {
        this.BeatmapID = BeatmapID;
    }

    public int getBeatmapSetID() {
        return BeatmapSetID;
    }

    public void setBeatmapSetID(int BeatmapSetID) {
        this.BeatmapSetID = BeatmapSetID;
    }

    public float getHPDrainRate() {
        return HPDrainRate;
    }

    public void setHPDrainRate(float HPDrainRate) {
        this.HPDrainRate = HPDrainRate;
    }

    public float getCircleSize() {
        return CircleSize;
    }

    public void setCircleSize(float CircleSize) {
        this.CircleSize = CircleSize;
    }

    public float getOverallDifficulty() {
        return OverallDifficulty;
    }

    public void setOverallDifficulty(float OverallDifficulty) {
        this.OverallDifficulty = OverallDifficulty;
    }

    public float getApproachRate() {
        return ApproachRate;
    }

    public void setApproachRate(float ApproachRate) {
        this.ApproachRate = ApproachRate;
    }

    public float getSliderMultiplier() {
        return SliderMultiplier;
    }

    public void setSliderMultiplier(float SliderMultiplier) {
        this.SliderMultiplier = SliderMultiplier;
    }

    public float getSliderTickRate() {
        return SliderTickRate;
    }

    public void setSliderTickRate(float SliderTickRate) {
        this.SliderTickRate = SliderTickRate;
    }

    public ArrayList getEventLines() {
        return EventLines;
    }

    public void setEventLines(ArrayList EventLines) {
        this.EventLines = EventLines;
    }

    public ArrayList getTimingPoints() {
        return TimingPoints;
    }

    public ArrayList<HitObject> getHitObjects() {
        return HitObjects;
    }

    public void setHitObjects(ArrayList<HitObject> HitObjects) {
        this.HitObjects = HitObjects;
    }

    public ArrayList<String> getColors() {
        return Colors;
    }

    public void setColors(ArrayList<String> Colors) {
        this.Colors = Colors;
    }

    public int getPhase() {
        return phase;
    }

    public void setPhase(int phase) {
        this.phase = phase;
    }

}
