BeatmapTweaker, version la plus r�cente : b0.5 - N�cessite java, d�velopp� pour Windows 7.

Installation :

1 - D�compresser l'archive en son int�gralit� dans un dossier et executer le .jar

Utilisation :

PARTIE 1 - EDITION D'UNE SEULE MAP

1 - S�lectionner n'importe quel fichier � l'int�rieur du dossier d'une beatmap

2 - S�lectionner une plusieurs difficult�s � �diter.
ATTENTION:
Le fichier audio doit �tre le m�me pour toutes les difficult�s s�lectionn�es, autrement la s�lection sera refus�e.
Il est possible de s�lectionner un groupe de difficult�s en maintenant la touche Maj (Shift)
Il est possible de s�lectionner plusieurs difficult�s en maintenant la touche Ctrl

3 - Choisir un pr�fixe pour les fichiers audio � cr�er (Ils seront nomm�s sous le format "pr�fixe" x"rate".mp3)

Par exemple, avec le pr�fixe de base "audio" et un rate de "1.2", le fichier sera nomm� "audio x1.2.mp3". Tout fichier nomm� de la sorte sera �cras�.

4 - Choisir un offset en cas de d�calage a r�gler.

Si la beatmap n'a pas de d�calage, alors il n'y en aura pas � la sortie.
En cas de d�calage, l'offset appliqu� le sera AVANT conversion.
Par exemple, si le fichier de base a un d�calage de 10ms, apr�s un rating de 1.2 le d�calage sera de 8,333ms. Pour le corriger, il faut entrer 10ms ou -10ms selon la nature du d�calage.

5 - Ajouter autant de rate que n�cessaire
ATTENTION:
Chaque rate sera appliqu� sur chaque difficult� s�lectionn�e au menu pr�c�dent.
Chaque rate entra�ne la conversion d'un fichier mp3, et est r�alis� simultan�ment avec les autres. Cette op�ration demande beaucoup de ressources et peut entra�ner un fort ralentissement du PC, d'une dur�e de 5 � 10 secondes d�s 4 rate.

6 - Inclure l'audio

Il est possible de g�n�rer uniquement les fichiers de beatmap et non les fichiers mp3.
Le fichier de beatmap sera tout de m�me reli� au fichier audio qui aurait du �tre cr�� (avec le format "pr�fixe" x"rate".mp3)

7 - Nightcore mode

Lorsque cette option est d�coch�e, l'effet appliqu� � l'audio sera similaire � l'effet "Double Time"
Lorsque cette option est coch�e, l'effet appliqu� � l'audio sera similaire � l'effet "Nightcore"

8 - Valider et d�marrer le traitement

Il faut laisser un d�lai avant de scanner les changements dans le jeu afin que les fichiers audio soient bien charg�s.

PARTIE 2 - CREATION DE PACKS

1 - Choisir les metadata de base du pack

2 - Pour chaque difficult� � ajouter au pack, choisir un fichier .osu et des rates � lui appliquer, de la m�me mani�re que pour une �dition de beatmap seule.

Questions :

Me poser des question sur discord si besoin : Adri#3336